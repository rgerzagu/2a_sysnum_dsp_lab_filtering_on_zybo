function Bruit=BruitMultQ(e1,e2,s,loi)

% Calcul du bruit g�n�r� en sortie de la multiplication
% Bruit=BruitMult(entr�e1,entr�e2,sortie)
%
% entr�e1,entr�e2,sortie: format des signaux d'entr�e et de sortie
% Bruit.var: variance du bruit

		Mult.N = e1.N + e2.N;	% Format en sortie du multiplieur

		kd=Mult.N-s.N;

		qd  = 2.^-(s.N);
      
        l=0;
        
  if(kd>0);

		switch loi
      	case 'AC', 	Bruit.mean =0;
							Bruit.var=((qd^2)/12)*(1+2^(-2*kd+1));
                     
        case {'A', 'A '},	Bruit.mean =(qd/2)*2^-(kd-l);
							Bruit.var=((qd^2)/12)*(1-2^(-2*(kd-l)));
         
      	case {'T', 'T '},	Bruit.mean =(qd/2)*(1-2^-(kd-l));
							Bruit.var=((qd^2)/12)*(1-2^(-2*(kd-l)));
      
      	otherwise, disp('Erreur d''utilisation de la fonction BruitMultQ(e1,e2,s,loi)');
            
      end
      
	else    
     
     Bruit.mean =0;
	  Bruit.var=0;
  end
      



	       
       