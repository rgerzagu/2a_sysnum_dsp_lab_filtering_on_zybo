function Bruit=BruitCadrageQ(e,s,loi)

% Calcul du bruit g�n�r� par un cadrage
% Bruit=BruitCadrage(entr�e1,entr�e2,sortie)
%
% entr�e1,entr�e2,sortie: format des signaux d'entr�e et de sortie
% Bruit.var: variance du bruit

		kd=e.N-s.N;

      qd  = 2.^-(s.N);
      
  if(kd>0)
            
      switch loi
      	case 'AC', 	Bruit.mean =0;
							Bruit.var=((qd^2)/12)*(1+2^(-2*kd+1));
                     
         	case {'A', 'A '},  	Bruit.mean =(qd/2)*2^-kd;
							Bruit.var=((qd^2)/12)*(1-2^(-2*kd));
         
      	case {'T', 'T '},  	Bruit.mean =(qd/2)*(1-2^-kd);
							Bruit.var=((qd^2)/12)*(1-2^(-2*kd));
      
      	otherwise, disp('Erreur d''utilisation de la fonction BruitCadrageQ(e,s,loi)');
            
      end
  else    
     
     Bruit.mean =0;
	  Bruit.var=0;
  end