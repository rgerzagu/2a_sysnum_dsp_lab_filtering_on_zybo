% --- cascadedFilter.m 
% --- 
% Description 
%    Creates a cascaded filter structure based on polynomial description of a filter 
%   It allows to transform a direct or transpose structure into Second Order Section (SoS) structure
%   The input is a IIR with L coerfficients and it returns a matrix Cx3 where 3 is the size of each cell (i.e 2 delay tap cells) and C is the number of cells required to create the filter
% --- 
% Syntax 
%    [NumCas,DenCas] = cascadedFilter(Num,Den)
%         % --- Input parameters 
%         Num : Numerator polynomial [1xL] 
%         Den : Denominator polynomial [1xL] 
%         % --- Output parameters 
%         NumCas : Matrix of numerator polynomials [Cx3]
%         DenCas : Matrix of Denominator polynomials [Cx3]
% --- 
% v 1.0 - Robin Gerzaguet.


function [NumCas,DenCas] = cascadedFilter(Num,Den)
   % --- Calculating Cascaded filter 
   [KCas,NumCas,DenCas] = dir2cas(Num,Den);
   % --- Scaling parameter 
   NumCas = ( KCas^0.25  ) .* NumCas;
   % --- Switch cells (Noise variance minimization)
   NumCas1 = NumCas;
   NumCas(1,:) = NumCas1(3,:);
   NumCas(2,:) = NumCas1(2,:);
   NumCas(3,:) = NumCas1(4,:);
   NumCas(4,:) = NumCas1(1,:);

   DenCas1 = DenCas;
   DenCas(1,:) = DenCas1(3,:);
   DenCas(2,:) = DenCas1(2,:);
   DenCas(3,:) = DenCas1(4,:);
   DenCas(4,:) = DenCas1(1,:);    
end

