%##########################################################################
%
%   Main .m : 
%
%   Fichier principal pour la conception du filtre IIR    
%
% 2020 - Robin Gerzaguet
%##########################################################################

% -------------------------------------
%% ---- Init environment and parameters 
% -------------------------------------
clc;
close all;
clear all;
addpath('./Fonctions/');
 
% --- Architecture parameters
DynIn	  = 1-2^(-15);  % input is Q(16,0,15)
Bmem	  = 16;         % Memory storage size 
Bmult	  = 32;         % Size of output of multiplier 
Badd	  = 32;         % Size of output and input of additionner (double precision)

% --- Filter parameters
Fe = 8000;           % Sampling frequency 
Te = 1/Fe;
Fp1 = 800;           % Begin of bandpass  
Fp2 = 1600;           % End of bandpass 
Fs1 = 720;           % Low frequency of stop band 
Fs2 = 1680;           % High frequency of stop band 
Wp = [2*Fp1/Fe 2*Fp2/Fe];   % Band pass
Ws = [2*Fs1/Fe 2*Fs2/Fe];   % Band stop
Rp = 3;                     % Ripple in bandpass 
Rs = 30;                    % Attenuation in bandstop 




% -------------------------------------
%% ---- Synthesis part 
% -------------------------------------
[Nf, Wn] = buttord(?, ?, ?, ?);      % Butterworth 
disp(strcat(sprintf('\n Filter Order - Butterworth : '), num2str(2 * Nf) ));

[Nf, Wn] = cheb1ord(?, ?, ?, ?);     % Chebyshev type I
disp(strcat(sprintf('\n Filter Order - Chebyshev type I : '), num2str(2 * Nf) ));   

[Nf, Wn] = cheb2ord(?, ?, ?, ?);    % Chebyshev type II
disp(strcat(sprintf('\n Filter Order - Chebyshev type II : '), num2str(2 * Nf) ));   

[Nf, Wn] = ellipord(?, ?, ?, ?);    % Elliptic 
disp(strcat(sprintf('\n Filter Order - Elliptique : '), num2str(2 * Nf) ));   

% --- Eliptic filter has lowest order, so let's design an elliptic filter
[Num,Den] = ellip(Nf, Rp, Rs, Wn);
% --- Cascaded filter 
[NumCas,DenCas] = cascadedFilter(Num,Den);

% -------------------------------------
%% ---- Filter analysis  
% -------------------------------------
% --- Parameters required for analysis 
Npts    = 128;                      % Support for evaluation
n       = 0 : Npts-1;
delta   = [1; zeros(Npts-1,1)];     % We create here an impulstion 
step    = ones(Npts,1);             % We create here a step
% --- Impulse response
h = 'To be completed';    % Have a look on impz function
figure; 
stem(n,h);
% --- Frequency response 
figure
L = 2^12;
[h,w] = freqz(Num,Den,L);       % Frequency analysis 
m = abs(h);			% Filter magnitude 
p = angle(h);				    % Filter phase
% --- Plotting stuff 
subplot(2,2,1); 
plot(w,20*log10(m)); 
title('Log Magnitude of the filter');
axis([0 pi -60 2]); 
grid
subplot(2,2,2);
plot(w,m); 
title('Magnitude (linear scale)'); 
subplot(2,2,3);
plot(w,p); 
title('Phase');
subplot(2,2,4);
zplane(Num,Den);
Zeros = roots(Num);
Poles = roots(Den);
% --- Template draw
NFp1  = round(L* Wp(1) );
NFp2  = round(L* Wp(2) );
NFs1  = round(L* Ws(1) );
NFs2  = round(L* Ws(2) );
gabh = [  -Rs * ones(NFs1,1);   0 * ones(NFs2-NFs1,1);   -Rs * ones(L-NFs2,1)];
gabl = [-1000 * ones(NFp1,1); -Rp * ones(NFp2-NFp1,1); -1000 * ones(L-NFp2,1)];
figure 
plot( w(1:L-1),20*log10(m(1:L-1)) ); 
title('Log Magnitude Filtre RII 8eme ordre'); 
axis([0 pi -60 2]); 
grid
hold on; 
plot(w,gabh,'r'); 
plot(w,gabl,'r');  

% -------------------------------------
%% ---- Filter coefficient analysis  
% -------------------------------------
% You will use here nbBits and FixCode, be sure taht you have properly code and test them.

% Format size 
figure 
TabBcoef = [8, 10, 12, 16, 18,20];
L = 2^12; 
for i = 1:length(TabBcoef)
	Bcoef  = TabBcoef(i); % Current word size 
    %-------------------------------------------------------------------------
    % Non cascaded structure 
	% Format defintion 
    % --- Numerator 
	b.B = Bcoef;			% Number of bits (Numertor)          
	b.M = '??';				% Integer part 
	b.N = b.B-b.M-1;	    % Fractional part 
    % --- Denominator
	a.B = Bcoef;            % Number of bits (Numertor)           
	a.M = '??';             % Integer part 
	a.N = a.B-a.M-1;        % Fractional part 
    % ---   Quantized values 
	NumFix = '??';
	DenFix = '??';
	% --- Modules of poles 
	TabPoles(i,:) = abs(roots(DenFix))';


	%--------------------------------------------------------------------------
	% Cascaded structure 
    % USE HERE THE CASCADED STRUCTURE (NumCas, DenCas)
	% Format defintion 
    % --- Numerator 
	b.B = Bcoef;			% Number of bits (Numertor)          
	b.M = '??';				% Integer part 
	b.N = b.B-b.M-1;	    % Fractional part 
    % --- Denominator
	a.B = Bcoef;            % Number of bits (Numertor)           
	a.M = '??';             % Integer part 
	a.N = a.B-a.M-1;        % Fractional part 

	% Quantized values
	NumFix1 ='??'; 
	NumFix2 ='??'; 
	NumFix3 ='??';
	NumFix4 ='??';

	DenFix1 ='??'; 
	DenFix2 ='??';
	DenFix3 ='??';
	DenFix4 ='??';


	% Transfert function (FP)
	H1 = tf(NumFix1,DenFix1 , Te, 'Variable','z^-1');   
	H2 = tf(NumFix2,DenFix2 , Te,'Variable','z^-1');   
	H3 = tf(NumFix3,DenFix3 , Te,'Variable','z^-1');   
	H4 = tf(NumFix4,DenFix4 , Te,'Variable','z^-1'); 

	Hd1 = tf(1,DenFix1 , Te,'Variable','z^-1');   
	Hd2 = tf(1,DenFix2 , Te,'Variable','z^-1');   
	Hd3 = tf(1,DenFix3 , Te,'Variable','z^-1');   
	Hd4 = tf(1,DenFix4 , Te,'Variable','z^-1'); 
	% Complete function 
	H = H1 * H2 * H3 * H4;
	[ NumFixCas , DenFixCas] = tfdata(H,'v');

	% Poles
	TabPolesCas(i,:) = abs(roots(DenFixCas))';


	%--------------------------------------------------------------------------
	% Display
	NFp1  = round(L* Wp(1) );
	NFp2  = round(L* Wp(2) );
	NFs1  = round(L* Ws(1) );
	NFs2  = round(L* Ws(2) );
	gabh = [  -Rs * ones(NFs1,1);   0 * ones(NFs2-NFs1,1);   -Rs * ones(L-NFs2,1)];
	gabl = [-1000 * ones(NFp1,1); -Rp * ones(NFp2-NFp1,1); -1000 * ones(L-NFp2,1)];

	subplot(3,2,i)
	[FreqResp,w] = freqz(NumFix,DenFix,L);        
	[FreqRespCas,w] = freqz(NumFixCas,DenFixCas,L); 
	[FreqId,wId] = freqz(Num,Den,L); 
	hold on; 
	plot( wId(1:10:L-1),20*log10( abs(FreqId(1:10:L-1)))); 
	plot( w(1:10:L-1),20*log10( abs(FreqResp(1:10:L-1)) ), 'r--'); 
	plot( w(1:10:L-1),20*log10( abs(FreqRespCas(1:10:L-1))),'g'); 
	legend('Designed','Single','Cascaded');
	title(strcat(strcat('Frequency response with  : ', num2str(Bcoef)),' bits')); 
	axis([0 pi -60 5]); 
	grid
    hold on; 
    %--- Template plot
    l1 = 2*pi * Fp1 / Fe ;
    l2 = 2*pi * Fp2 / Fe ;
    u1 = 2*pi * Fs1 / Fe ;
    u2 = 2*pi * Fs2 / Fe ;
    plot([0 u1],-[Rs Rs],'color','black');
    plot([u1 u1],[-Rs 0],'color','black');
    plot([u1 u2],[0 0],'color','black');
    plot([u2 u2],[-Rs 0],'color','black');
    plot([u2 2*pi],-[Rs Rs],'color','black');
    plot([0 l1],[-180 -180],'color','black');
    plot([l1 l1],[-180 -Rp],'color','black');
    plot([l1 l2],[-Rp -Rp],'color','black');
    plot([l2 l2],[-180 -Rp],'color','black');
    plot([l2 2*pi],-[180 180],'color','black');
end

% Affichage du module des poles 

disp('Module des poles des filtres cascad�s')
disp(TabPolesCas)

disp('Module des poles du filtre non-cascad�')
disp(TabPoles)


% -------------------------------------
%% ---- Output format calculation  
% -------------------------------------
% --- Recreate transfert function 
Hn = tf(Num,1 , Te, 'Variable','z^-1');   % Hn(z) = Y(z)/D(z)
Hd = tf(1 ,Den, Te, 'Variable','z^-1');   % Hd(z) = D(z)/X(z)
H   = Hd * Hn;                         % H(z)  = Y(z)/X(z)

% --- Creating data for norm L1 norm 
TMax    = 20;
MyL1 = NormL1(H, TMax, DynIn);
disp('y(n) dynamic using L1 norm is '); disp(MyL1)

% --- Chebyshev norm 
TMax    = 1e3;
MyC = NormChebychev(H, TMax, DynIn);
disp('y(n) dynamic using Chebyshev norm'); disp(MyC)

% --- Final norm choice 
My = '???';  % Is it MyL1 or MyC ?

% ---------------------------------------------------- 
%% --- Direct structure analysis   
% ---------------------------------------------------- 
% Here we create all the FP design for the direct structure non-cascaded 
% This structure is to be used with the Simulink diagram flow. In this diagram, all the operation will be done 
% using floating point and fixed point, and the quantization error will be computed afterwards.
% ===> Have a look on IIR8Directe.mdl

% In this cell all the intermediate formats have to be defined in a structure used for calculation in the Simulink part 

% Filter 
Mb = '???';		  % Dynamique: Integer part of numerator 
Ma = '???';		  % Dynamique: Integer part of denominator
% Input 
Mx = 0;           % Format of input => x is Q(16,0,15)
% Multipliers
Mz1 = '???';            % Integer part of Multiplier for numerator ?
Mz2 = '???';      % Integer part of Multiplier for denominator ?
% Adders 
Madd = '???';    % Integer part of common adder 



% --- Complete format defintion 
% Casting numerator 
b.B = Bmem;                 % Word size 
b.M = Mb;                   % Integer part 
b.N = b.B-b.M-1;            % Fractional part 
b.format = sfrac(b.B,b.M);  % Simulink structure 
AfficheFormat('   Format of b : ', b);

% Casting denominator
a.B = Bmem;                 % Word size          
a.M = Ma;                   % Integer part       
a.N = a.B-a.M-1;            % Fractional part    
a.format = sfrac(a.B,a.M);  % Simulink structure 
AfficheFormat('   Format of a : ', a);

% Input // Ouptut formats
x.B = Bmem;
x.M = Mx;
x.N = x.B-x.M-1;
x.format = sfrac(x.B,x.M);
AfficheFormat('   Format of x : ', x);
y.B = Bmem;
y.M = My;
y.N = y.B - y.M-1;
s1.format=sfrac(y.B,y.M);
AfficheFormat('   Format of y : ', y);

% --- Intermediate variables 
% x * b
smb1.B = Bmult;          % Output of  multiplication x*b   
smb1.M = Mz1;
smb1.N = smb1.B-smb1.M-1;
smb1.format = sfrac(smb1.B,smb1.M);
AfficheFormat('   Format of MULT1 (x*b) : ', smb1);
% a * y
sma1.B = Bmult;          % Output of  multiplication a*y   
sma1.M = Mz2;
sma1.N = sma1.B-sma1.M-1;
sma1.format=sfrac(sma1.B,sma1.M);
AfficheFormat('   Format of MULT2 (y*a) : ', sma1);
% +
sadd1.B = Bmult;
sadd1.M = Madd;
sadd1.N =sadd1.B-sadd1.M-1;
sadd1.format = sfrac(sadd1.B,sadd1.M);
AfficheFormat('   Format of ADD : ', sadd1);


% Coefficient in FP for the direct structure  
NumFix = '???';     % Define numerator in FP (direct structure) 
DenFix = '???';     % Define denominator in FP (direct structure) 

% --- Simulink routine 
sim('IIR8Directe');              % Floating point and fixed point simulation 
disp('MSE at the output of the direct structure');
disp(10*log10(var(y_flottant-y_fix)/var(y_flottant)))  % Compute MSE 


% ---------------------------------------------------- 
%% --- Cascaded structure analysis   
% ---------------------------------------------------- 
% Here we create all the FP design for the cascaded structure non-cascaded 
% This structure is to be used with the Simulink diagram flow. In this diagram, all the operation will be done 
% using floating point and fixed point, and the quantization error will be computed afterwards.
% ===> Have a look on IIRCascadeDirecte.mdl

% In this cell all the intermediate formats have to be defined in a structure used for calculation in the Simulink part 
% Filter 
Mb = '???';		  % Dynamique: Integer part of numerator 
Ma = '???';		  % Dynamique: Integer part of denominator
% Input 
Mx = 0;           % Format of input => x is Q(16,0,15)
% Multipliers
Mz1 = '???';            % Integer part of Multiplier for numerator ?
Mz2 = '???';      % Integer part of Multiplier for denominator ?
% Adders 
Madd = '???';    % Integer part of common adder 


% --- Complete format defintion 
% Casting Numerator 
b.B = Bmem;                 % Word size 
b.M = Mb;                   % Integer part 
b.N = b.B-b.M-1;            % Fractional part 
b.format = sfrac(b.B,b.M);  % Simulink structure 
AfficheFormat('   Format of b : ', b);

% Casting denominator
a.B = Bmem;                 % Word size          
a.M = Ma;                   % Integer part       
a.N = a.B-a.M-1;            % Fractional part    
a.format = sfrac(a.B,a.M);  % Simulink structure 
AfficheFormat('   Format of a : ', a);

% Input // Ouptut formats
x.B = Bmem;
x.M = Mx;
x.N = x.B-x.M-1;
x.format = sfrac(x.B,x.M);
AfficheFormat('   Format of x : ', x);
y.B = Bmem;
y.M = My;
y.N = y.B - y.M-1;
s1.format=sfrac(y.B,y.M);
AfficheFormat('   Format of y : ', y);

% --- Intermediate variables 
% x * b
smb1.B = Bmult;          % Output of  multiplication x*b   
smb1.M = Mz1;
smb1.N = smb1.B-smb1.M-1;
smb1.format = sfrac(smb1.B,smb1.M);
AfficheFormat('   Format of MULT1 (x*b) : ', smb1);
% a * y
sma1.B = Bmult;          % Output of  multiplication a*y   
sma1.M = Mz2;
sma1.N = sma1.B-sma1.M-1;
sma1.format=sfrac(sma1.B,sma1.M);
AfficheFormat('   Format of MULT2 (y*a) : ', sma1);
% +
sadd1.B = Bmult;
sadd1.M = Madd;
sadd1.N =sadd1.B-sadd1.M-1;
sadd1.format = sfrac(sadd1.B,sadd1.M);
AfficheFormat('   Format of ADD : ', sadd1);


% Numerator
NumFix1 = '???'; 
NumFix2 = '???'; 
NumFix3 = '???'; 
NumFix4 = '???';
% Denominator
DenFix1 = '???';
DenFix2 = '???'; 
DenFix3 = '???'; 
DenFix4 = '???'; 

% The format are the same as before, only coefficients are different.

% --- Simulink routine 
sim('IIRCascadeDirecte');        % Floating point and fixed point simulation 
disp('MSE at the output of the cascaded structure');
disp(10*log10(var(y_flottant-y_fix)/var(y_flottant)))  % Compute MSE




