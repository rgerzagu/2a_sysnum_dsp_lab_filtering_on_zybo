% Equivalent to runtests.sh 
% Do all the tests
function check_NbBits()
    disp("-----------------------------")
    disp("--- Unitary tests for NbBits ")
    disp("-----------------------------")

    % --- Scalar 
    disp("--- SCALAR ")
    ALL = 0;
    NB  = 0;
    [eS,f] = nutshell_nbBits(12.5,4); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(2.5,2); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(-12.5,4); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(0,0); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(137,8); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(-140,8); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(256,9); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(-256,9); disp(eS); ALL = ALL + f; NB = NB + 1;
    disp(['=========> ' num2str(ALL) ' tests passed on ' num2str(NB)]);

    disp("--- Vector ")
    ALL = 0;
    NB  = 0;
    [eS,f] = nutshell_nbBits(rand(1,100),0); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(rand(100,100),0); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(-rand(1,100),0); disp(eS); ALL = ALL + f; NB = NB + 1;
    x = [0;0;-1225;123;12];
    [eS,f] = nutshell_nbBits(x,11); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_nbBits(zeros(1,100),0); disp(eS); ALL = ALL + f; NB = NB + 1;
    disp(['=========> ' num2str(ALL) ' tests passed on ' num2str(NB)]);



end

% Nutshell test function
% Create the output and check it matches the theory
function [eS,flag] = nutshell_nbBits(x,mT)
    m   = NbBits(x) ;
    flag = 0;
    if length(x) > 1 
        l = 'Array'; 
    else 
        l = num2str(x);
    end
    if m ~= mT 
        eS = ['/!\ Error with NbBits on ' l ': Obtain ' num2str(m) ' instead of ' ,num2str(mT) ' bits'];
    else 
        flag = 1;
        eS = ['OK- Test Pass for ' l '; Obtain ' num2str(m) 'bits'];
    end
end



