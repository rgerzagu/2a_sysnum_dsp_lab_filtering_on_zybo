% Equivalent to runtests.sh 
% Do all the tests
function check_FixCode()
    disp("-----------------------------")
    disp("--- Unitary tests for FixCode ")
    disp("-----------------------------")

    % --- Scalar 
    disp("--- SCALAR ")
    ALL = 0;
    NB  = 0;
    [eS,f] = nutshell_FixCode(12.5,12.5); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_FixCode(2.5,2.5); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_FixCode(-12.5,-12.5); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_FixCode(0,0); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_FixCode(-12.1351355,-12.125); disp(eS); ALL = ALL + f; NB = NB + 1;
    [eS,f] = nutshell_FixCode(-12.1151355,-12.125); disp(eS); ALL = ALL + f; NB = NB + 1;

    disp(['=========> ' num2str(ALL) ' tests passed on ' num2str(NB)]);

    disp("--- VECTOR ")
    ALL = 0;
    NB  = 0;
    [eS,f] = nutshell_FixCode(zeros(1,10),zeros(1,10)); disp(eS); ALL = ALL + f; NB = NB + 1;
    x = [-0.125,0,-12.1247,7.84,156.7];
    [eS,f] = nutshell_FixCode(x,[0,0,-12.1247,7.84, 156.7]); disp(eS); ALL = ALL + f; NB = NB + 1;
    x = [-0.125,0;-12.1247,7.84;156.7,78.45];
    [eS,f] = nutshell_FixCode(x,[0,0;-12.1247,7.84; 156.7,78.5]); disp(eS); ALL = ALL + f; NB = NB + 1;

    disp(['=========> ' num2str(ALL) ' tests passed on ' num2str(NB)]);



end

% Nutshell test function
% Create the output and check it matches the theory
function [eS,flag] = nutshell_FixCode(x,yT)
    y   = FixCode(x,NbBits(x)) ;
    flag = 0;
    if y ~= yT 
        eS = ['/!\ Error with FixCode: Obtain ' num2str(y) ' instead of ' ,num2str(yT) ' for ' num2str(x) ];
    else 
        flag = 1;
        eS = ['OK- Test Pass: Obtain ' num2str(y(:)') ' as truncation of ' num2str(x(:)')];
    end
end



