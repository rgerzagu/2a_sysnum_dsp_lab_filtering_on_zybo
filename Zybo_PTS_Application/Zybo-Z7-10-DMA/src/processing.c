// Processing functions
#include "processing.h"
#include <unistd.h>


/*
Apply IIR filter to input array 
- Input is int16_t 
- Output is u32 to be compliant with stereo system (2 times 16 bits)
*/
int iirFilter(uint32_t* out,int16_t* in , int size){// Temp variable as input/output 
    /*--------------------------------------------------------------------------
    * --- Init variables
    * --------------------------------------------------------------------------*/
    // Temp variables 
    int16_t x,y;
    int32_t tmp;  // Adder @ 32 bits
    // Init filter cells 
    int16_t A[N][2],B[N][3];
    // Circular buffer (Ring buffer) used to age data (flip flop)
    int16_t dX[N][3],dY[N][2];
    // Loop variables 
    int16_t k,j;  // For cells
    int n;        // For time index. Should be on 32 bits as lots of points

    /*--------------------------------------------------------------------------
    * --- Init filter coefficients 
    * --------------------------------------------------------------------------*/

    /* Numerator */
    /* Numerator is on (16,0,15) */
    /* Numerator */
    /* Numerator is on (16,0,15) */
    B[0][0] = 
    B[0][1] = 
    B[0][2] = 
    B[1][0] = 
    B[1][1] = 
    B[1][2] = 
    B[2][0] = 
    B[2][1] = 
    B[2][2] = 
    B[3][0] = 
    B[3][1] = 
    B[3][2] = 

    /* Denominator  is on (16,1,14) */
    A[0][0] =
    A[0][1] =
    A[1][0] =
    A[1][1] =
    A[2][0] =
    A[2][1] =
    A[3][0] =
    A[3][1] =

  // Init delay 
    for (k=0;k<N;k++)
    {
        for (j=0;j<3;j++)
        { 
            // To be completed
        }
        for (j=0;j<2;j++)
        { 
            // To be completed
        }
    }    

    // Filtering 
    for(n=0; n< size; n++)
    {

        // X is the input signal
        x = in[n]; 

        for(k=0;k<N;k++)
        {    
            /* Setting entry */
            /* Entry is (16,0,15), no need to recast */
            dX[k][0] = x;

            /* FIR left size: entry x is multiplied by numerator */ 
            /* x is (16,0,15)  and B is (16,0,15) */
            /* tmp becomes (xx,xx,xx) -> Need to switch to (xx,xx,xx) for next step */
            tmp =	; // To be completed . Use multipler32 for double precision multiplication 


            /* FIR Right side: entry y is delayed and multiplied */ 
            /* Result is on (32,2,29) */ 
            /* A is on (xx,xx,xx) and Y is (xx,xx,xx) -> (xx,xx,xx) : */
            tmp += ; // To be completed 

            /* Ouptut is on long (xx,xx,xx) -> Switch to int (xx,xx,xx); */
            /* After shifting, casting on (xx,xx,xx) (classic int) */
            y = (int16_t)(tmp  >> XX); // To be completed  

            /* Updating old values for input and output */
            /* All d values are on (16,0,15) */

            /* Next entry is current output */ 
            x = y; 			

        }
        /* Output */
        // Output is y a Q(16,0,15) that is finally converted to a stereo u32 for audio rendering.
        out[2*n] = (uint32_t)(y);
        out[2*n+1] = (uint32_t)(y);
    }
    return 0;
}


// A custom multiplication operator to be sure we use double precision multiplication 
int32_t multiplier32(int16_t a, int16_t b)
{
    int32_t result;
    result = ((int32_t) a) * ((int32_t) b);
    return result;
}
