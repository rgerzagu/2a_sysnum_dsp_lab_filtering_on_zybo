/*
 * processing.h
 *
 *  Created on: 11 janv. 2021
 *      Author: robin
 */

#ifndef SRC_PROCESSING_H_
#define SRC_PROCESSING_H_

#include <stdio.h>
#include <math.h>


/* Macros */
#define N 4         // Number of cells 

/* Prototypes */
int iirFilter(uint32_t* out, int16_t* in, int size);
int32_t multiplier32(int16_t a, int16_t b);


#endif /* SRC_PROCESSING_H_ */
