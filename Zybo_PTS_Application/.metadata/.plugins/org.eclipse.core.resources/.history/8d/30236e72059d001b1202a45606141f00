/************************************************************************/
/*	 Main function call 
/       Minimal code to instantiate all the stuff and do the processing 																	*/
/************************************************************************/
/************************** Run mode  ******************************/
#define RUN_MODE 0
// 0 ==> Direct audio rendering mode: Audio output is the ideal signal (sweep)
// 1 ==> Filtering audio rendering mode : Audio output is the filtered signal
/*******************************************************************/

// Main call prototype to hide glue for platform description
#include "main.h"

/* ---- Platform design header --- */
#include "audio/audio.h"
#include "dma/dma.h"
#include "iic/intc.h"
#include "userio/userio.h"
#include "iic/iic.h"

/* --- Zynq 7010 header files --- */
#include "xaxidma.h"
#include "xparameters.h"
#include "xil_exception.h"
#include "xdebug.h"
#include "xiic.h"
#include "xaxidma.h"
#include "xtime_l.h"

/* --- Sleep system  --- */
#include "xscugic.h"
#include "sleep.h"
#include "xil_cache.h"

/* --- Including user space functions  --- */
#include "printers/printers.h"
#include "timers/timers.h"
#include "kissFFT/kiss_fft.h"
#include "data_file.h"
#include "processing.h"

/* --- Constant definition  --- */
// Audio constants
// Number of seconds to record/playback
#define NR_SEC_TO_REC_PLAY 5
// ADC/DAC sampling rate in Hz
#define AUDIO_SAMPLING_RATE 44000 // (44kHz stero)
// Number of samples to record/playback
#define NR_AUDIO_SAMPLES (NR_SEC_TO_REC_PLAY * AUDIO_SAMPLING_RATE)
// Timeout loop counter for reset
#define RESET_TIMEOUT_COUNTER 10000
#define TEST_START_VALUE 0x0

/* --- Other definition  --- */
#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif
static XIic sIic;
static XAxiDma sAxiDma; /* Instance of the XAxiDma */
static XGpio sUserIO;
static XScuGic sIntc;
// Interrupt vector table
const ivt_t ivt[] = {
    //IIC
    {XPAR_FABRIC_AXI_IIC_0_IIC2INTC_IRPT_INTR, (Xil_ExceptionHandler)XIic_InterruptHandler, &sIic},
    //DMA Stream to MemoryMap Interrupt handler
    {XPAR_FABRIC_AXI_DMA_0_S2MM_INTROUT_INTR, (Xil_ExceptionHandler)fnS2MMInterruptHandler, &sAxiDma},
    //DMA MemoryMap to Stream Interrupt handler
    {XPAR_FABRIC_AXI_DMA_0_MM2S_INTROUT_INTR, (Xil_ExceptionHandler)fnMM2SInterruptHandler, &sAxiDma},
    //User I/O (buttons, switches, LEDs)
    {XPAR_FABRIC_AXI_GPIO_0_IP2INTC_IRPT_INTR, (Xil_ExceptionHandler)fnUserIOIsr, &sUserIO}};

int main(void)
{
    /*--------------------------------------------------------------------------
    * --- Init all stuff 
    * --------------------------------------------------------------------------*/
    // Do nothing here !!
    int Status;
    Demo.u8Verbose = 1;
    printf("\n--- Entering main() --- \n");

    //Initialize the interrupt controller
    Status = fnInitInterruptController(&sIntc);
    if (Status != XST_SUCCESS)
    {
        printf("Error initializing interrupts");
        return XST_FAILURE;
    }
    // Initialize IIC controller
    Status = fnInitIic(&sIic);
    if (Status != XST_SUCCESS)
    {
        printf("Error initializing I2C controller");
        return XST_FAILURE;
    }
    // Initialize User I/O driver
    Status = fnInitUserIO(&sUserIO);
    if (Status != XST_SUCCESS)
    {
        printf("User I/O ERROR");
        return XST_FAILURE;
    }
    //Initialize DMA
    Status = fnConfigDma(&sAxiDma);
    if (Status != XST_SUCCESS)
    {
        printf("DMA configuration ERROR");
        return XST_FAILURE;
    }
    //Initialize Audio I2S
    Status = fnInitAudio();
    if (Status != XST_SUCCESS)
    {
        printf("Audio initializing ERROR");
        return XST_FAILURE;
    }
    // Waiting to be sure HW is ready
    {
        XTime tStart, tEnd;
        XTime_GetTime(&tStart);
        do
        {
            XTime_GetTime(&tEnd);
        } while ((tEnd - tStart) / (COUNTS_PER_SECOND / 10) < 20);
    }
    //Initialize Audio I2S
    Status = fnInitAudio();
    if (Status != XST_SUCCESS)
    {
        printf("Audio initializing ERROR");
        return XST_FAILURE;
    }
    // Enable all interrupts in our interrupt vector table
    fnEnableInterrupts(&sIntc, &ivt[0], sizeof(ivt) / sizeof(ivt[0]));

    /*--------------------------------------------------------------------------
    * --- Processing
    * --------------------------------------------------------------------------*/
    printf("----------------------------------------------------------\n");
    printf("Zybo Z7-10 DMA Audio Demo\n");
    printf("----------------------------------------------------------\n");
    Demo.fDmaMM2SEvent = 0;
    Demo.fDmaS2MMEvent = 0;

    u32 *ptr0 = (u32 *)(MEM_BASE_ADDR);
    if (RUN_MODE == 0) {
        // --- Direct Audio output
        // We have to fill the buffer @ ptr0 by data
        // For the moment, the output buffer is set to be the same as the input buffer
        // Define output buffer location ==> This is set to be the DMA address
        // Output is on u32 as u16 x2 (stereo mode)
        for (int i = 0; i < SIZE; i++)
        {
            ptr0[i] = (u32)(sig[i]);
        }
    }
    else {
        // Apply IIR filter 
        iirFilter(ptr0,sig,SIZE);
    }

    // --- Payback
    printf("Play 5 second signals \n");
    fnSetHpOutput();
    fnAudioPlay(sAxiDma, NR_AUDIO_SAMPLES);
    // Checking the DMA MM2S event flag
    u8 waitPlay = 1;
    while (waitPlay)
    {
        if (Demo.fDmaMM2SEvent)
        {
            printf("\nPayback done \n");
            // Disable Stream function to send data (S2MM)
            Xil_Out32(I2S_STREAM_CONTROL_REG, 0x00000000);
            Xil_Out32(I2S_TRANSFER_CONTROL_REG, 0x00000000);
            //Reset MM2S event and playback flag
            Demo.fDmaMM2SEvent = 0;
            Demo.fAudioPlayback = 0;
            Xil_DCacheFlushRange((u32)MEM_BASE_ADDR, 4 * NR_AUDIO_SAMPLES); //FIXME 4*NR
            waitPlay = 0;
        }
    }
    // Print the result into the UART
    //u32 *ptr = (u32 *)(MEM_BASE_ADDR + XAXIDMA_BUFFLEN_OFFSET);
    //print_array_u32(ptr, 5 * 96000, "res");
    //printf("\n--- End of Simulation --- \n");

    /*--------------------------------------------------------------------------
    * --- Recording  
    * --------------------------------------------------------------------------*/
    printf("\n--- End of Simulation --- \n");
    return XST_SUCCESS;
}
